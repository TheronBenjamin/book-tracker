import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageProjectListComponent} from "./pages/page-project-list/page-project-list.component";

const routes: Routes = [
  { path: '', pathMatch:"full", redirectTo: 'list' },
  { path: 'list', pathMatch:"full", component: PageProjectListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
