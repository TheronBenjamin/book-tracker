import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRoutingModule } from './project-routing.module';
import { PageProjectListComponent } from './pages/page-project-list/page-project-list.component';


@NgModule({
  declarations: [
    PageProjectListComponent
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule
  ]
})
export class ProjectModule { }
