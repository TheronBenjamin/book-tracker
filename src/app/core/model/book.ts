import {BookI} from "../interface/book-i";

export class Book implements BookI{
  id = 0;
  title = "";
  description = "";
  author = "";
  year = "";

  constructor(obj?: Partial<Book>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
