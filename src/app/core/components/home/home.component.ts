import {Component, OnInit} from '@angular/core';
import VanillaTilt from "vanilla-tilt";
import gsap from "gsap";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    let vanillaEl: any = document.querySelectorAll('.vanilla-tilt') as any;
    VanillaTilt.init(vanillaEl);
    gsap.from(".test", {ease: "power2", y: -2000, duration: 1.2})
    gsap.from(".about-me", {ease: "power4", x: 200, duration: 1.4})
    gsap.from(".stack-card", {ease: "power4", x: -200, duration: 1.4})
    gsap.from(".project-link", {ease: "back", y: 15000, duration: 1.2})
    }
}
