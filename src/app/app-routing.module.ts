import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ContactComponent} from "./core/components/contact/contact.component";
import {HomeComponent} from "./core/components/home/home.component";

const routes: Routes = [

    { path:'', pathMatch:'full', component: HomeComponent},
    { path:'contact', component: ContactComponent},
    { path: 'books', loadChildren : () =>
        import('./book/book.module')
          .then((module_) => module_.BookModule)
    },
    { path: 'projects', loadChildren: ()  =>
        import('./project/project.module')
          .then((module_) => module_.ProjectModule)
    }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
