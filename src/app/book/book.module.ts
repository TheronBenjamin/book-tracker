import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookRoutingModule } from './book-routing.module';
import { PageAddBookComponent } from './pages/page-add-book/page-add-book.component';
import { PageEditBookComponent } from './pages/page-edit-book/page-edit-book.component';
import { PageListBookComponent } from './pages/page-list-book/page-list-book.component';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatCardModule} from "@angular/material/card";
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [
    PageAddBookComponent,
    PageEditBookComponent,
    PageListBookComponent
  ],
  imports: [
    CommonModule,
    BookRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    SharedModule,
  ]
})
export class BookModule { }
