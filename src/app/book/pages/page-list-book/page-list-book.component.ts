import { Component, OnInit } from '@angular/core';
import {BookService} from "../../service/book.service";
import {Book} from "../../../core/model/book";
import {Observable} from "rxjs";
import VanillaTilt from "vanilla-tilt";

@Component({
  selector: 'app-page-list-book',
  templateUrl: './page-list-book.component.html',
  styleUrls: ['./page-list-book.component.scss']
})
export class PageListBookComponent implements OnInit {

  public books$!: Observable<Book[]>;

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.getAllBooks();
  }

  getAllBooks(){
    this.books$ = this.bookService.getAllBooks();

    // let vanillaEl: any = document.querySelectorAll('.book-card');
    // console.log(vanillaEl);
    // VanillaTilt.init(vanillaEl);
  }

  deleteBook(book: Book){
    if (confirm("Are you sure you want to delete this book ? " + book.title))
      this.bookService.deleteBook(book).subscribe(() => this.getAllBooks());
  }

}
