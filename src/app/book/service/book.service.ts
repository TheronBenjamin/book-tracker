import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Book} from "../../core/model/book";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private url: string = 'http://localhost:3000/books'

  constructor(private http: HttpClient) { }

  getAllBooks(): Observable<Book[]>{
    return this.http.get<Book[]>(this.url)
  }

  deleteBook(book: Book): Observable<Book>{
    return this.http.delete<Book>(this.url + '/' + book.id);
  }

}
