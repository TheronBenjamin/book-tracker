import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageListBookComponent} from "./pages/page-list-book/page-list-book.component";
import {PageAddBookComponent} from "./pages/page-add-book/page-add-book.component";
import {PageEditBookComponent} from "./pages/page-edit-book/page-edit-book.component";

const routes: Routes = [
  { path:'', pathMatch:'full', redirectTo:'list'},
  { path: 'list', component: PageListBookComponent },
  { path: 'add', component: PageAddBookComponent },
  { path: 'edit/:id', component: PageEditBookComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoutingModule { }
