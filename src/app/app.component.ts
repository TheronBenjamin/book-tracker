import { Component } from '@angular/core';
import gsap from "gsap";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'task_tracker';


  constructor() { }

  ngOnInit(): void {
    const circleSvg = document.querySelector('svg');
    const btn = document.querySelector('button');
    let mouseX = "";
    let mouseY = "";

    window.addEventListener('mousemove', (event) => {
      const pxDivided = 16;
      mouseX = (event.pageX / pxDivided )- (45/pxDivided) + 'rem';
      mouseY = (event.pageY / pxDivided )- (45/pxDivided) + 'rem';
    });

    const mousMove = () => {
      if(circleSvg != null) {
        circleSvg.style.top = mouseY;
        circleSvg.style.left = mouseX;
      }
      window.requestAnimationFrame(mousMove);
    }

    mousMove();

    var t1 = gsap.timeline({defaults: {ease: 'power2.inOut'}});
    t1.to(circleSvg, {width: 0, opacity: 0});
    t1.to('.cursor, button', {height: '0vh'});
    t1.pause();
    if(btn != null){
      btn.addEventListener('click', () => {
        t1.play();
      })
    }
   }
}
