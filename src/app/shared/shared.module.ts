import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {RouterModule} from "@angular/router";
import { SearchbarComponent } from './searchbar/searchbar.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import { FooterComponent } from './footer/footer.component';



@NgModule({
    declarations: [
        MenuComponent,
        SearchbarComponent,
        FooterComponent
    ],
  exports: [
    MenuComponent,
    SearchbarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule
  ]
})
export class SharedModule { }
