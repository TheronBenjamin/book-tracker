import { Component, OnInit } from '@angular/core';
import gsap from "gsap";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  Home: string ="Home";

  constructor() { }

  ngOnInit(): void {
    gsap.from(".link1", {ease: "power2", x: -500, duration: .7})
    gsap.from(".link2", {ease: "power2", x: 120, duration: 1.4})
    gsap.from(".link3", {ease: "power2", x: 160, duration: 1.6})
    gsap.from(".link4", {ease: "power2", x: 200, duration: 1.8})
  }

}
